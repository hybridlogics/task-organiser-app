export class User{
  _id:string;
  first_name:string;
  last_name:string;
  avatar:string;
  phone:string;
  type:string;
  childern:Array<User>;

  constructor(){

  }
}
