import { Component } from '@angular/core';
import { NavController, NavParams,Events } from 'ionic-angular';
import {DomSanitizer} from '@angular/platform-browser';
import {Storage} from '@ionic/storage';
import {ProfilePage} from '../profile/profile';
import {AuthService} from '../../providers/auth-service';
import {UserFeedback} from '../../utilities/user-feedback'
import { MyApp } from '../../app/app.component';
import { UtilityService } from '../../providers/utility-service';
/*
  Generated class for the Login page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
  providers:[AuthService,UserFeedback]
})
export class SettingPage {
  user:any={};
  constructor(private events:Events,private userFeedback:UserFeedback,private authService:AuthService,private storage:Storage,private domSanitizer:DomSanitizer,public navCtrl: NavController, public navParams: NavParams,public utilityService: UtilityService) {
    this.storage.get("userInfo").then(user=>{
      this.user=user;
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    MyApp.i = 0;
    this.utilityService.decrease_badge();

  }

  editProfile(){
    this.navCtrl.push(ProfilePage);
  }
  /*
  deleteProfile(){
    this.userFeedback.loader.present();
    this.authService.delete_account(this.user._id).then(rs=>{
      this.userFeedback.loader.dismiss();
      this.userFeedback.make_toast("Marked as Done");
      this.events.publish("new_event",{
        event_name:"delete_account",
        id:this.user._id,
        username:`${this.user.first_name} ${this.user.last_name}`,
      });
    })
  }*/

  ionViewDidLeave()
  {
    MyApp.i = 0;
    this.utilityService.decrease_badge();
  }

}
