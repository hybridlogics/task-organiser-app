export class Task{
  _id:string;
  points:number=0;
  title:string;
  desc:string;
  child:string;
  parent:string;
  deadline:number;
  next_time:number;
}
