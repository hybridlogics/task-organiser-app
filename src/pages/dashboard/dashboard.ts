import { Component } from '@angular/core';
import { MenuController, NavController, NavParams, Events, LoadingController, Platform } from 'ionic-angular';
import { CreateTaskPage } from '../create-task/create-task';
import { AddChildPage } from '../add-child/add-child';
import { TaskDetailsPage } from '../task-details/task-details';
import { InvitationPage } from '../invitation/invitation';

import { ChildReportPage } from '../child-report/child-report';

import { Storage } from '@ionic/storage';
import { UtilityService } from '../../providers/utility-service';
import { UserFeedback } from '../../utilities/user-feedback';

import { DomSanitizer } from '@angular/platform-browser';
import { BrowserTab } from '@ionic-native/browser-tab';
import { MyApp } from '../../app/app.component';

import { Push, PushToken, providePush} from '@ionic/cloud-angular';
//import { Push, PushObject, PushOptions} from '@ionic-native/push';

/*
  Generated class for the Dashboard page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/


@Component({
  templateUrl: 'dashboard.html',
  providers: [UtilityService, BrowserTab]
})
export class DashboardPage {
  personal_childern: Array<any> = [];
  work_childern: Array<any> = [];
  user: any;
  points: number = 0;
  current_adv: string;
  current_adv_link: string;
  index: number = 1;
  data: any;
  allchildren: any;
  flag: any;
  flag1: any;
  static counter: number;
  j: any;
  reebott: any;
 

  constructor(private browserTab: BrowserTab, private userFeedback: UserFeedback, private utilityService: UtilityService, public push: Push, public storage: Storage, private _DomSanitizationService: DomSanitizer, public navCtrl: NavController, public navParams: NavParams, public event: Events, menuCtrl: MenuController, public loadingCtrl: LoadingController, public platform: Platform) {

    this.flag1 = true;
    // this.j = JSON.parse(localStorage.getItem("appreboot"));  
    DashboardPage.counter = 0;
    this.flag = true;
    this.storage.set('fbprofile', 'false');
    let no_check = navParams.get("no_check");
    this.get_adverts();
    menuCtrl.enable(true, "left");
    storage.get('userInfo').then(data => {
      this.user = data;

      this.utilityService.get_appreboot(this.user._id).then(data => {
        this.reebott = data;
        this.j = this.reebott.reboot;
        console.log('appreboot value', this.j);
        if (this.j > 0) {
          this.flag1 = false;
        }
      }).catch(err => { console.log('error while geting reboot number') })

      // this.utilityService.get_appreboot(this.user._id).subscribe(data => {
      //   this.j = data.reboot;
      //   console.log('appreboot value',this.j);
      //   if (this.j > 0) {
      //     this.flag1 = false;
      //   }
      // }, err => , () => console.log('success get'));

      if (!no_check == undefined)
        this.check_invitations();
      if (!data.dev_token)
        this.get_send_user_token();
      setTimeout(() => { this.get_childern() }, 3000);
    }).catch(err => { });

    MyApp.i = 0;
    this.utilityService.decrease_badge();

//     document.addEventListener('resume', () => {
//       this.push.unregister();
//   });

//   document.addEventListener('pause', () => {
//     this.push.register().then((t: PushToken) => {
//       this.utilityService.update_token(t.token, this.user._id);
//     })
// });

MyApp.i = 0;
this.utilityService.decrease_badge();

  }

  check_invitations() {
    this.utilityService.get_invitations(this.user._id).then((invitations: any) => {
      if (invitations.length > 0)
        this.navCtrl.setRoot(InvitationPage, {
          invitations: invitations
        });
    })
  }

  ngOnInit() {
    this.event.subscribe('task_approved', (data) => {
      this.get_childern();
    console.log('task_approved awasdwasdwaw');
    });

    this.event.subscribe('create_task', (data) => {
      this.get_childern();
    console.log('task_approved awasdwasdwaw');
    });

    this.event.subscribe('task_done', (data) => {
      this.get_childern();
    console.log('task_done awasdwasdwaw');
    });

    this.event.subscribe('task_chat', (data) => {
      this.get_childern();
    console.log('task_chat awasdwasdwaw');
    });

    this.event.subscribe('accept_invitation', (data) => {
      this.userFeedback.generate_notification(1, data.username + " accepted your invitation");
      this.get_childern();
    });
  }

  onPageWillLeave() {
    this.event.unsubscribe('task_approved');
    this.event.unsubscribe('accept_invitation');
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter in dashboard');
    this.get_childern();
  }

  // refreshh() {
  //   console.log('asdas')
  //   let loader = this.loadingCtrl.create({
  //     content: "Please wait...",
  //   });
  //   loader.present();
  //   this.get_childern();
  //   loader.dismiss();
  // }

  get_childern(refresher = undefined) {

    this.utilityService.getChildern(this.user._id).subscribe(data => {
      if (refresher)
        refresher.complete();
      this.allchildren = data;
      // if (this.allchildren.length >= 2) {
      //   this.flag1 = false;
      // }
      console.log(this.allchildren);
      this.user.childeren = data;
      this.storage.set('userInfo', this.user);

      this.work_childern = data.filter(child => {
        return child.type == "work";
      }).map(child => {
        return child.id;
      });

      if (this.work_childern.length == 0) {
        this.flag = false;
      }

    },
      err => console.log("Error while getting childs"), () => console.log("success getimg childs"));

  }
  //   this.utilityService.getChildern(this.user._id).then((rs: any) => {
  //     if (refresher)
  //       refresher.complete();

  //     console.log(rs.childeren);
  //     this.allchildren = rs.childeren;
  //     // this.personal_childern=rs.childeren.filter(child=>{
  //     //   return child.type=="personal";
  //     // }).map(child=>{
  //     //     return child.id;
  //     // });
  //     // this.personal_childern.unshift(this.user);
  //     // this.work_childern=rs.childeren.filter(child=>{
  //     //   return child.type=="work";
  //     // }).map(child=>{
  //     //     return child.id;
  //     // });
  //     this.user.childeren = rs.childeren;
  //     this.storage.set('userInfo', this.user);
  //   }).catch(err => { });
  // }

  get_points() {
    this.utilityService.getPoints(this.user._id).then((rs: any) => {
      this.points = rs.points;
      this.data.points = this.points;
      this.storage.set('userInfo', this.data);
    })
  }
  get_adverts() {
    this.utilityService.get_adverts().subscribe(adverts => {
      this.current_adv = "data:image/png;base64," + adverts[0].data;
      this.current_adv_link = adverts[0].advert_link;
      setInterval((self, length) => {
        if (self.index == length)
          self.index = 0;
        this.current_adv = "data:image/png;base64," + adverts[self.index].data
        this.current_adv_link = adverts[self.index].advert_link;
        self.index++;
      }, 10000, this, adverts.length)
    });
  }
  

  get_send_user_token() {
    this.push.register().then((t: PushToken) => {
      this.utilityService.update_token(t.token, this.user._id);
    })
    this.push.rx.notification()
      .subscribe((msg: any) => {
        this.get_childern();
        this.get_points();
      }); 
  }


  
  createTask() {
    this.navCtrl.push(CreateTaskPage);
  }

  addChild() {
    this.navCtrl.push(AddChildPage);
  }

  childReport(child_id, avatar, points) {
    this.navCtrl.push(ChildReportPage, {
      child_id: child_id,
      avatar: avatar,
      points: points
    });
  }

  open_url(link) {
    this.browserTab.openUrl(link);
  }

  doRefresh(event) {
    this.get_childern(event);
  }

  ionViewDidLeave()
  {
    MyApp.i = 0;
    this.utilityService.decrease_badge();
  }

}