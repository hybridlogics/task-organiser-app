import { Component, ViewChild } from '@angular/core';
import { Nav, NavController, Platform, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { DomSanitizer } from '@angular/platform-browser';
import { HomePage } from '../pages/home/home';
import { SocialLoginPage } from '../pages/social-login/social-login';
import { ProfilePage } from '../pages/profile/profile';
import { SettingPage } from '../pages/setting/setting';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { CreateTaskPage } from '../pages/create-task/create-task';
import { AddChildPage } from '../pages/add-child/add-child';
import { RecurringTaskPage } from '../pages/recurring-task/recurring-task';
import { ChildReportPage } from '../pages/child-report/child-report';
import { InvitePage } from '../pages/invite/invite';
import { InvitationPage } from '../pages/invitation/invitation';
import { UserFeedback } from '../utilities/user-feedback';
import { User } from '../Models/User';
import { Storage } from '@ionic/storage';
import { AuthService } from '../providers/auth-service';
import { TaskDetailsPage } from '../pages/task-details/task-details';
import { UtilityService } from '../providers/utility-service';
import { Badge } from '@ionic-native/badge';
import * as io from 'socket.io-client';
import { Push, PushToken } from '@ionic/cloud-angular';
import { BackgroundMode } from '@ionic-native/background-mode';



@Component({
  templateUrl: 'app.html',
  providers: [UserFeedback, AuthService]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  userInfo: User = new User();
  pages: Array<{ title: string, component: any }>;
  socket: any;
  user: any = {};
  static i: number = 0;
  j: number = 0;
  reboot: number = 0;
  reebott: any;
  pendingrequest: number = 0;

  constructor(private utilityService: UtilityService, private authService: AuthService, public userFeedback: UserFeedback, public storage: Storage, public platform: Platform, public statusBar: StatusBar, public push: Push, private _DomSanitizationService: DomSanitizer, public splashScreen: SplashScreen, public event: Events, private badge: Badge, private backgroundMode: BackgroundMode) {

    // if (localStorage.getItem("appreboot") != undefined) {
    //   this.j = JSON.parse(localStorage.getItem("appreboot"));
    // }

    // this.j = this.j + 1;
    // localStorage.setItem("appreboot", JSON.stringify(this.j))

    //http://97.74.4.85:3000

    this.backgroundMode.enable();

    this.socket = io.connect("http://97.74.4.85:3000");
    this.socket.on('connect', function () {
      if (this.user) {
        this.socket.emit('logout', this.user._id);
        this.socket.emit('join', this.user._id);
      }
    });

    this.invitationapi(this.userInfo._id);

    this.socket.on("new_event", msg => {

      this.utilityService.increase_badgee();
      this.invitationapi(this.userInfo._id);

      DashboardPage.counter = 0;
      switch (msg.event_name) {
        case 'new_invitation':
          this.nav.push(InvitationPage, { user_id: this.userInfo._id });
          this.userFeedback.generate_notification(4, `Add request from ${msg.from_fn} ${msg.from_ln}`);
          break;
        case 'task_approved':
          this.userFeedback.generate_notification(1, `${msg.task} completion approved`, 0, { id: msg.task_id, avatar: this.user.avatar });
          break;
        case 'task_approved_not_done':
          this.userFeedback.generate_notification(1, `${msg.task} completion approved (Not Done)`, 0, { id: msg.task_id, avatar: this.user.avatar });
          break;
        case 'task_delete':
          this.userFeedback.generate_notification(4, `${msg.username} delete task ${msg.task}`);
          break;
        case 'task_done':
          this.userFeedback.generate_notification(3, `${msg.username} marked ${msg.task} as done`, 0, { id: msg.task_id, avatar: msg.avatar });
          break;
        case 'create_task':
          this.userFeedback.generate_notification(5, `New task from ${msg.username}`, msg.sender_id);
          break;
        case 'task_chat':
          if (this.userInfo._id != msg.user_id)
          this.userFeedback.generate_notification(5, `New chat from ${msg.username}`, 0, { id: msg.task_id, avatar: msg.avatar });
          break;
        case 'update_task':
          this.userFeedback.generate_notification(6, `${msg.username} update task ${msg.task}`);
          break;
        case 'not_complete':
          this.userFeedback.generate_notification(7, `${msg.task} completion disapproved`, 0, { id: msg.task_id, avatar: this.user.avatar });
          break;
        case 'declined_invitation':
          this.userFeedback.generate_notification(7, `${msg.username} declined your add request`);
          break;
        case 'accept_invitation':
          this.userFeedback.generate_notification(1, msg.username + " accepted your invitation");
          break;

      }
      event.publish(msg.event_name, msg);
    });

    event.subscribe('new_event', (event) => {
      this.socket.emit('new_event', event);
    });

    event.subscribe('afterLogin', (user) => {
      this.userInfo = user;
      this.socket.emit('join', user._id);
    });

    this.initializeApp();

    this.pages = [
      { title: 'Home', component: DashboardPage },
      { title: 'Add User', component: AddChildPage },
      { title: 'Invite User', component: InvitePage },
      // { title: 'Pending requests', component: InvitationPage },
      // { title: 'Settings', component: SettingPage },
      // { title: 'Log Out', component: null },

    ];

    event.subscribe('get_task_from_toast', (task) => {
      let page = this.nav.getActive();
      if (task.info) {
        if (page.component.name != 'TaskDetailsPage' && page.data.task_id != task.info.id) {
          this.nav.push(TaskDetailsPage, {
            task_id: task.info.id,
            child_avatar: task.info.avatar
          });
        }
      } else {
        this.utilityService.get_task_by_parent(task.parent_id, task.child_id).then(task => {
          if (task)
            if (page.component.name != 'TaskDetailsPage' && page.data.task_id != task[0]._id) {
              this.nav.push(TaskDetailsPage, {
                task_id: task[0]._id,
                child_avatar: this.user.avatar
              });
            }
        });
      }
    });

    storage.get("userInfo").then((data) => {
      this.user = data;
      if (data) {
        if (!data.phone) {
          this.rootPage = ProfilePage, { fbprofile: true };
        } else {

          this.utilityService.get_appreboot(data._id).then(dataa => {
            this.reebott = dataa;
            this.reboot = this.reebott.reboot;
            this.invitationapi(data._id);
            console.log('reboot value in app component before', this.reboot);
            this.reboot = this.reboot + 1;
            console.log('reboot value in app component after', this.reboot);

            this.utilityService.set_appreboot(data._id, this.reboot).then(data => {
            }).catch(err => { console.log('error while geting reboot number') })

          }).catch(err => { console.log('error while geting reboot number') })

          // this.utilityService.get_appreboot(data._id).subscribe(data => {
          //   this.reboot = data.reboot;
          //   console.log('reboot value in app component before', this.reboot);
          //   this.reboot = this.reboot + 1;
          //   console.log('reboot value in app component after', this.reboot);
          //   this.utilityService.set_appreboot(data._id, this.reboot).subscribe(data => {
          //   }, err => console.log('error while setting reboot number'), () => console.log('success set'));
          // }, err => console.log('error while geting reboot number'), () => console.log('success get'));
          
          this.rootPage = DashboardPage;
          event.publish("afterLogin", data);
        }
      } else {
        this.rootPage = SocialLoginPage;
      }
    });

  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if (page.component == null) {
      this.storage.clear();
      if (this.user) {
        this.authService.remove_token(this.user._id).then(() => { });
        this.event.publish("new_event", {
          event_name: "logout",
          user: this.user._id
        });
      }
      this.nav.setRoot(SocialLoginPage);
    } else {
      this.nav.push(page.component);
    }
  }

  pendingPage() {
    this.nav.push(InvitationPage);
  }
  settingPage() {
    this.nav.push(SettingPage);
  }
  logout() {
    this.storage.clear();
    if (this.user) {
      this.authService.remove_token(this.user._id).then(() => { });
      this.event.publish("new_event", {
        event_name: "logout",
        user: this.user._id
      });
    }
    this.nav.setRoot(SocialLoginPage);
  }

  invitationapi(userid) {
    this.utilityService.countinvitation(userid).then(dataa => {
      this.pendingrequest = dataa;
    }).catch(err => { console.log('error while geting reboot number') })
  }
}
