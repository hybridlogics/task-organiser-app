import { Component } from '@angular/core';
import { MenuController, NavController, NavParams, LoadingController, Events } from 'ionic-angular';
import { SignUpPage } from '../sign-up/sign-up';
import { DashboardPage } from '../dashboard/dashboard';
import { HomePage } from '../home/home';

import { ProfilePage } from '../profile/profile';
import { AuthService } from '../../providers/auth-service';
import { UtilityService } from '../../providers/utility-service';
import { Facebook, GooglePlus } from 'ionic-native';
import { Storage } from '@ionic/storage';
import { UserFeedback } from '../../utilities/user-feedback';
import { BrowserTab } from '@ionic-native/browser-tab';
import { Push, PushToken } from '@ionic/cloud-angular';

/*
  Generated class for the SocialLogin page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-social-login',
  templateUrl: 'social-login.html',
  providers: [AuthService, UserFeedback, UtilityService, BrowserTab]
})
export class SocialLoginPage {
  dev_token: string;
  loader: any;
  reboot: number = 0;
  reebott: any;

  user: any = { key: '', password: '' };
  constructor(private event: Events, private browserTab: BrowserTab, public push: Push, private utilityService: UtilityService, private userFeedback: UserFeedback, public loadingCtrl: LoadingController, public storage: Storage, public navCtrl: NavController, public navParams: NavParams, public authService: AuthService, menuCtrl: MenuController) {
    menuCtrl.enable(false, "left");
    this.loader = this.loadingCtrl.create({
      content: "Please wait...",
    });

    storage.get('countries').then(counties => {
      if (!counties)
        this.utilityService.get_countries().subscribe(counties => {
          storage.set('countries', counties);
        })
    })

  }

  ionViewDidLoad() {
    Facebook.browserInit(1917879508469115, "v2.9").catch(error => { });
  }

  signUp(event: Event) {
    event.preventDefault();
    this.navCtrl.push(SignUpPage);
  }

  reset(event: Event) {
    event.preventDefault();
    this.navCtrl.push(HomePage);
  }


  //LOFo9SKN7F8bAO1yWQNggTVIZOA
  doFacebookLogin() {
    Facebook.logout();
    //,'user_birthday'
    this.storage.clear();
    let permissions = ['public_profile', 'email'];//,'user_birthday'
    let self = this;
    Facebook.login(permissions).then(function (response) {
      console.log(JSON.stringify(response))
      let params = new Array();
      Facebook.api('/me?fields=id,first_name,last_name,gender,picture.width(500).height(500),email', params).then(function (user) { //,birthday
        user.dev_token = self.dev_token;
        user.avatar = user.picture.data.url;
        user._id = user.id;
        // if(user.birthday){
        //   let parts=user.birthday.split("/");
        //   user.birth_day=parts[0];
        //   user.birth_month=parts[1];
        //   user.birth_year=parts[2];
        // }
        delete user.picture;
        self.loader.present();
        self.authService.createAccount(user, 'social').then((result: any) => {
          self.loader.dismiss();
          if (!result.error && result != undefined) {
            self.storage.set("userInfo", result).then(() => {
              self.event.publish("afterLogin", result);
              if (!result.phone) {

                this.utilityService.get_appreboot(result._id).then(dataa => {

                  this.reebott = dataa;
                  this.reboot = this.reebott.reboot;
                  console.log('reboot value in app component before', this.reboot);
                  this.reboot = this.reboot + 1;
                  console.log('reboot value in app component after', this.reboot);

                  this.utilityService.set_appreboot(result._id, this.reboot).then(data => {
                  }).catch(err => { console.log('error while geting reboot number') })

                }).catch(err => { console.log('error while geting reboot number') })
                self.navCtrl.push(ProfilePage, { fbprofile: true })
              }
              else

                this.utilityService.get_appreboot(result._id).then(dataa => {

                  this.reebott = dataa;
                  this.reboot = this.reebott.reboot;
                  console.log('reboot value in app component before', this.reboot);
                  this.reboot = this.reboot + 1;
                  console.log('reboot value in app component after', this.reboot);

                  this.utilityService.set_appreboot(result._id, this.reboot).then(data => {
                  }).catch(err => { console.log('error while geting reboot number') })

                }).catch(err => { console.log('error while geting reboot number') })

              self.navCtrl.setRoot(DashboardPage)
            });
          } else {
          }
        });
      }).catch((error) => {
        // this.userFeedback.alert("ERROR",JSON.stringify(error));
      })
    }).catch(r => {
      // this.userFeedback.alert("ERROR",JSON.stringify(r));
    })
  }

  doGoogleLogin() {
    let self = this;
    self.loader.present();
    GooglePlus.login({ scopes: "profile" }).then((gplusData) => {
      let user: any = { last_name: gplusData.familyName, first_name: gplusData.givenName, _id: gplusData.userId, email: gplusData.email, avatar: gplusData.imageUrl, dev_token: self.dev_token }
      self.authService.getGoogleInfo(user._id).then((googleUser: any) => {
        user.gender = googleUser.gender;
        // if(googleUser.birthday){
        //   let parts=googleUser.birthday.split("-");
        //   user.birth_day=parts[2];
        //   user.birth_month=parts[1];
        //   user.birth_year=parts[0];
        // }
        self.authService.createAccount(user, 'social').then((result: any) => {
          self.loader.dismiss();
          if (!result.error) {
            self.storage.set("userInfo", result).then(() => {
              self.event.publish("afterLogin", result);
              if (!result.phone) {
                this.utilityService.get_appreboot(result._id).then(dataa => {

                  this.reebott = dataa;
                  this.reboot = this.reebott.reboot;
                  console.log('reboot value in app component before', this.reboot);
                  this.reboot = this.reboot + 1;
                  console.log('reboot value in app component after', this.reboot);

                  this.utilityService.set_appreboot(result._id, this.reboot).then(data => {
                  }).catch(err => { console.log('error while geting reboot number') })

                }).catch(err => { console.log('error while geting reboot number') })
                self.navCtrl.push(ProfilePage)

              }
              else {
                this.utilityService.get_appreboot(result._id).then(dataa => {

                  this.reebott = dataa;
                  this.reboot = this.reebott.reboot;
                  console.log('reboot value in app component before', this.reboot);
                  this.reboot = this.reboot + 1;
                  console.log('reboot value in app component after', this.reboot);

                  this.utilityService.set_appreboot(result._id, this.reboot).then(data => {
                  }).catch(err => { console.log('error while geting reboot number') })

                }).catch(err => { console.log('error while geting reboot number') })


                self.navCtrl.setRoot(DashboardPage)
              }


            });
          }
        });
      })
    },
      (gplusErr) => {
        self.loader.dismiss();
        //alert("gplusErr:"+JSON.stringify(gplusErr));
      });
  }

  login() {
    this.loader.present();
    this.storage.clear();
    this.authService.login(this.user).then(result => {
      this.loader.dismiss();
      if (result.login != false) {
        this.storage.set("userInfo", result).then(() => {
          this.event.publish("afterLogin", result);
        });
        if (!result.phone) {
          this.utilityService.get_appreboot(result._id).then(dataa => {

            this.reebott = dataa;
            this.reboot = this.reebott.reboot;
            console.log('reboot value in app component before', this.reboot);
            this.reboot = this.reboot + 1;
            console.log('reboot value in app component after', this.reboot);

            this.utilityService.set_appreboot(result._id, this.reboot).then(data => {
            }).catch(err => { console.log('error while geting reboot number') })

          }).catch(err => { console.log('error while geting reboot number') })
          this.navCtrl.setRoot(ProfilePage)
        } else {

          this.storage.get('userInfo').then(data => {
            this.user = data;

            this.utilityService.get_appreboot(this.user._id).then(dataa => {

              this.reebott = dataa;
              this.reboot = this.reebott.reboot;
              console.log('reboot value in app component before', this.reboot);
              this.reboot = this.reboot + 1;
              console.log('reboot value in app component after', this.reboot);

              this.utilityService.set_appreboot(this.user._id, this.reboot).then(data => {
              }).catch(err => { console.log('error while geting reboot number') })

            }).catch(err => { console.log('error while geting reboot number') })


            this.push.register().then((t: PushToken) => {
              this.utilityService.update_token(t.token, this.user._id);
            })
            this.push.rx.notification()
              .subscribe((msg: any) => {
              });

          }).catch(err => { });

          this.navCtrl.setRoot(DashboardPage)

        }
      } else {
        this.userFeedback.make_toast("invalid credentials");
      }
    })
  }

  terms() {
    this.browserTab.openUrl("http://www.taskorganiser.co.uk/terms.html");
  }
}
