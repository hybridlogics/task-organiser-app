import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { UserFeedback } from '../../utilities/user-feedback';
import { ProfilePage } from '../profile/profile';
import { FormBuilder, FormGroup } from '@angular/forms';
import { EmailValidator } from '../../validators/EmailValidator';
import { UtilityService } from '../../providers/utility-service';
import { MyApp } from '../../app/app.component';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [UserFeedback, UtilityService]
})
export class HomePage {
  resetForm: FormGroup;
  emailValidator: EmailValidator;
  days: Array<number> = [];
  months: Array<number> = [];
  years: Array<number> = [];
  day: any;
  month: any;
  year: any;
  DOB: any;

  constructor(private utilityService: UtilityService, private formBuilder: FormBuilder, private userFeedback: UserFeedback, public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
    this.emailValidator = new EmailValidator(utilityService, 0);
    this.resetForm = formBuilder.group({
      email: ['', this.emailValidator.isValid, this.emailValidator.isEmailExists],
      birth_day: ['1'],
      birth_month: ['1'],
      birth_year: ['2017']
    });

    for (let i = 1; i <= 12; i++) {
      this.months.push(i);
    }
    for (let i = 1; i <= 31; i++) {
      this.days.push(i);
    }
    for (let i = new Date().getFullYear(); i >= new Date().getFullYear() - 117; i--) {
      this.years.push(i);
    }

    MyApp.i = 0;
    this.utilityService.decrease_badge();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  reset() {

    this.day = this.resetForm.controls['birth_day'].value;
    this.month = this.resetForm.controls['birth_month'].value;
    this.year = this.resetForm.controls['birth_year'].value;
    console.log(this.day, this.month, this.year);

    this.utilityService.DOBbyEmail(this.resetForm.value.email).subscribe(data => {
      this.DOB = data;
      console.log(this.DOB);

      if (this.day === this.DOB.day && this.month === this.DOB.month && this.year === this.DOB.year) {
        this.userFeedback.loader.present();
        this.utilityService.reset_password(this.resetForm.value.email).then(result => {
          this.userFeedback.loader.dismiss();
          this.userFeedback.make_toast("Reset password email sent");
        }, err => this.userFeedback.loader.dismiss())
          .catch(err => this.userFeedback.loader.dismiss())
      }else
      {
        this.showAlert();
      }

    },
      err => console.log("Error while getDOB"), () => console.log("success get DOB"));

    // this.userFeedback.loader.present();
    // this.utilityService.reset_password(this.resetForm.value.email).then(result=>{
    //   this.userFeedback.loader.dismiss();
    //   this.userFeedback.make_toast("Reset password email sent");
    // },err=>this.userFeedback.loader.dismiss())
    // .catch(err=>this.userFeedback.loader.dismiss())
  }

  check_date() {
    let user = this.resetForm.value;
    let date = new Date(user.birth_month + "/" + (user.birth_day) + "/" + user.birth_year);
    this.resetForm.controls['birth_day'].setValue(date.getDate());
    this.resetForm.controls['birth_month'].setValue(date.getMonth() + 1);
    this.resetForm.controls['birth_year'].setValue(date.getFullYear());

  }

  showAlert() {
    let alert = this.alertCtrl.create({
      title: 'Date of Birth Error!',
      subTitle: 'Please Select Correct Date of Birth!',
      buttons: ['OK']
    });
    alert.present();
  }
  
  ionViewDidLeave()
  {
    MyApp.i = 0;
    this.utilityService.decrease_badge();
  }
  
}
