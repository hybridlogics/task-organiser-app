import { FormControl } from '@angular/forms';
import { UtilityService } from '../providers/utility-service';
import { Http,Headers } from '@angular/http';

export class EmailValidator {
    utitly:UtilityService;

    constructor(public utilityService:UtilityService,public id:any){
      EmailValidator.prototype.utitly=utilityService;
      EmailValidator.prototype.id=id;
    }
   isValid(control: FormControl): any {
      var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
      if(!EMAIL_REGEXP.test(control.value)){
          return { "Please provide a valid email": true };
      }
      return null;
    }

    isEmailExists(control: any):any{
      let self=EmailValidator.prototype;
      return new Promise((resolve)=>{
        self.utitly.http.get("http://97.74.4.85:3000/users/exists/"+control.value).map(resp=>resp.json()).subscribe(result=>{
          if(result.exists)
            resolve(null)
          else
            resolve({"Please provide exists email": true})
        })
      })
    }

    isEmailExistsForProfile(control: any):any{
      let self=EmailValidator.prototype;
      return new Promise((resolve)=>{
        self.utitly.http.get("http://97.74.4.85:3000/users/exists_profile/"+control.value+"/"+self.id).map(resp=>resp.json()).subscribe(result=>{
          if(!result.exists)
            resolve(null)
          else
            resolve({"Please provide unique email": true})
        })
      })
    }
}
