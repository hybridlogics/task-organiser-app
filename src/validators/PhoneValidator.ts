import { FormControl } from '@angular/forms';
import { AuthService } from '../providers/auth-service';


export class PhoneValidator {
    utitly:AuthService;
    constructor(public authService:AuthService,public id:number){
      PhoneValidator.prototype.utitly=authService;
      PhoneValidator.prototype.id=id;
    }


    isPhoneExists(control: any):any{
      let self=PhoneValidator.prototype;
      return new Promise((resolve)=>{
        self.utitly.http.get("http://97.74.4.85:3000/users/phone/"+control.value+"/"+self.id).map(resp=>resp.json()).subscribe(result=>{
          if(!result.exists)
            resolve(null)
          else
            resolve({"Please provide unique phone": true})
        })
      })
    }
}
