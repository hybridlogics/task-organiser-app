import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Badge } from '@ionic-native/badge';
import { MyApp } from '../app/app.component';


@Injectable()
export class UtilityService {

  headers: Headers;
  api_url: string;

  constructor(public http: Http, private badge: Badge) {
    this.headers = new Headers({ 'Content-Type': 'application/json' });
  }


  addChild(parent, child, parent_id, relation_type, username) {
    //     return this.http.get("http://97.74.4.85:3000/invitations/add/"+parent+"/"+child+"/"+parent_id+"/"+relation_type+"/"+username).map(resp=>resp.json()).toPromise();
    // }
    return this.http.get("http://97.74.4.85:3000/invitations/add/" + parent + "/" + child + "/" + parent_id + "/" + relation_type + "/" + username).map(resp => resp.json()).toPromise();
  }

  get_invitations(child_id) {
    return new Promise(resolve => {
      this.http.get("http://97.74.4.85:3000/invitations/get/" + child_id).map(resp => resp.json()).subscribe(data => {
        resolve(data);
      });
    })
  }


  performInvitation(invitation_id, code) {
    return this.http.get("http://97.74.4.85:3000/invitations/perform/" + invitation_id + "/" + code).map(resp => resp.json());
  }
  delete_task(task_id) {
    return this.http.get("http://97.74.4.85:3000/tasks/delete/" + task_id);
  }

  // getChildern(parent) {
  //   return new Promise(resolve => {
  //     this.http.get("http://97.74.4.85:3000/users/get_childern/" + parent).map(resp => resp.json()).subscribe(data => {
  //       resolve(data);
  //     });
  //   })
  // }

  getChildern(parent) {
    return this.http.get("http://97.74.4.85:3000/users/get_childern/" + parent).map(res => res.json());
  }


  getPoints(user_id) {
    return new Promise(resolve => {
      this.http.get("http://97.74.4.85:3000/users/get_points/" + user_id).map(resp => resp.json()).subscribe(data => {
        resolve(data);
      });
    })
  }


  createTask(task) {
    return new Promise(resolve => {
      this.http.post("http://97.74.4.85:3000/tasks/create", JSON.stringify(task), { headers: this.headers }).map(resp => resp.json()).subscribe(data => {
        resolve(data);
      });
    })
  }

  recurringTask(task) {
    return this.http.post("http://97.74.4.85:3000/tasks/recurring", JSON.stringify(task), { headers: this.headers }).map(resp => resp.json());
  }

  updateTask(task) {
    return new Promise(resolve => {
      this.http.post("http://97.74.4.85:3000/tasks/update", JSON.stringify(task), { headers: this.headers }).map(resp => resp.json()).subscribe(data => {
        resolve(data);
      });
    })
  }

  getTasks(child, parent, status, yearMonth) {
    return this.http.get("http://97.74.4.85:3000/tasks/get/" + child + "/" + parent + "/" + status + "/" + yearMonth).map(resp => resp.json());
  }


  getTaskDetails(task_id) {
    return new Promise(resolve => {
      this.http.get("http://97.74.4.85:3000/tasks/details/" + task_id).map(resp => resp.json()).subscribe(data => {
        resolve(data);
      });
    })
  }

  task_done(child_id, task_id, child, task, toid) {
    return new Promise(resolve => {
      this.http.get("http://97.74.4.85:3000/tasks/done/" + child_id + "/" + task_id + "/" + child + "/" + task + "/" + toid).map(resp => resp.json()).subscribe(data => {
        resolve(data);
      });
    })
  }

  approve(task_id, points, child_id, title, user_id) {
    return new Promise(resolve => {
      this.http.get("http://97.74.4.85:3000/tasks/approve/" + task_id + "/" + points + "/" + child_id + "/" + title + "/" + user_id).map(resp => resp.json()).subscribe(data => {
        resolve(data);
      });
    })
  }

  notapprove(task_id, points, child_id, title, user_id) {
    return new Promise(resolve => {
      this.http.get("http://97.74.4.85:3000/tasks/not_approve/" + task_id + "/" + points + "/" + child_id + "/" + title + "/" + user_id).map(resp => resp.json()).subscribe(data => {
        resolve(data);
      });
    })
  }

  not_complete(task_id, child_id, parent_name, title) {
    return this.http.get(`http://97.74.4.85:3000/tasks/not_complete/${task_id}/${child_id}/${parent_name}/${title}`).map(resp => resp.json()).toPromise();
  }

  get_friends_accounts(numbers, user_id) {
    return new Promise(resolve => {
      this.http.post("http://97.74.4.85:3000/users/accounts/", JSON.stringify({ numbers: numbers, id: user_id }), { headers: this.headers }).map(resp => resp.json()).subscribe(data => {
        resolve(data);
      });
    })
  }

  get_adverts() {
    return this.http.get("http://97.74.4.85:3000/portal/adverts_json").map(resp => resp.json());
  }

  get_countries() {
    return this.http.get("http://97.74.4.85:3000/portal/country").map(resp => resp.json());
  }


  update_token(token, user_id) {
    this.http.get("http://97.74.4.85:3000/users/token/" + user_id + "/" + token).subscribe(data => {
    });
  }

  add_note(note, task_id, user_id, child_id) {
    return this.http.post("http://97.74.4.85:3000/tasks/add_note/" + child_id + "/" + user_id, JSON.stringify({
      note: note,
      task_id: task_id,
      user_id: user_id
    }), { headers: this.headers });
  }

  reset_password(email) {
    return this.http.get("http://97.74.4.85:3000/users/reset_password/" + email).map(resp => resp.json()).toPromise();
  }

  get_task_by_parent(parent_id, child_id) {
    return this.http.get(`http://97.74.4.85:3000/tasks/get_task_by_parent/${parent_id}/${child_id}`).map(resp => resp.json()).toPromise();
  }

  isEmailExists1(email) {
    return this.http.get("http://97.74.4.85:3000/users/exists/" + email).map(res => res.json());
  }

  chatfalse(userid, childid, taskid) {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post("http://97.74.4.85:3000/tasks/messagered/" + userid + "/" + childid + "/" + taskid,
      { headers: headers })
      .map(res => res.json());
  }


  DOBbyEmail(email) {
    return this.http.get("http://97.74.4.85:3000/users/dob/" + email).map(res => res.json());
  }

  get_appreboot(userid) {

    return new Promise(resolve => {
      this.http.get("http://97.74.4.85:3000/users/reboot/" + userid).map(resp => resp.json()).subscribe(data => {
        resolve(data);
      });
    })
  }

  set_appreboot(userid, number) {

    return new Promise(resolve => {
      this.http.post("http://97.74.4.85:3000/users/reboot/" + userid + "/" + number, { headers: this.headers }).map(resp => resp.json()).subscribe(data => {
        resolve(data);
      });
    })

    //   var headers = new Headers();
    //   headers.append('Content-Type', 'application/json');

    //   return this.http.post("http://97.74.4.85:3000/users/reboot/"+userid+"/"+number,
    //     { headers: headers })
    //     .map(res => res.json());
  }

  countinvitation(user_id) {
    return this.http.get("http://97.74.4.85:3000/invitations/countInvitations/" + user_id).map(resp => resp.json()).toPromise();
  }

  task_read_fasle(taskid, userid) {

    return new Promise(resolve => {
      this.http.post("http://97.74.4.85:3000/tasks/boldTaskChild/" + taskid + "/" + userid, { headers: this.headers }).map(resp => resp.json()).subscribe(data => {
        resolve(data);
      });
    })
  }

  increase_badgee() {
      this.badge.increase(MyApp.i++);
      this.badge.set(MyApp.i); 
  }

  decrease_badge()
  {
    if (MyApp.i == 0) {
      this.badge.set(MyApp.i);
    } else {
      this.badge.decrease(MyApp.i--);
      this.badge.set(MyApp.i);
    }
  }

}
