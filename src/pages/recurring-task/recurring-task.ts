import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DatePicker } from '@ionic-native/date-picker';
import { UtilityService } from '../../providers/utility-service';
import {UserFeedback} from '../../utilities/user-feedback';
import {DashboardPage} from '../dashboard/dashboard'
import { MyApp } from '../../app/app.component';
/*
  Generated class for the RecurringTask page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-recurring-task',
  templateUrl: 'recurring-task.html',
  providers:[DatePicker,UtilityService,UserFeedback]
})
export class RecurringTaskPage {
  period:number=31;
  number:number=1;
  numbers:Array<number>=[];
  start_date:any;
  end_date:any;
  task:any;
  constructor(private userFeedback:UserFeedback,private utilityService:UtilityService,public navCtrl: NavController, public navParams: NavParams,private datePicker: DatePicker) {
    this.period_change();
    this.task=navParams.get("task");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RecurringTaskPage');
  }
  period_change(){
    this.numbers=[];
    this.number=1;
    for (let i = 1; i <= this.period; i++) {
        this.numbers.push(i);
    }
  }

  start_date_change(value){
    if(value=="custom"){
      this.setDate("start");
    }
  }

  end_date_change(value){
    if(value=="custom"){
      this.setDate("end");
    }
  }

  setDate(type:string){
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT
    }).then(
      date => {
        if(type=="start"){
          this.start_date=date;
        }else if(type=="end"){
          this.end_date=date;
        }
      },
      err => {}
    );

    MyApp.i = 0;
    this.utilityService.decrease_badge();
  }

  setRecurring(){
    this.userFeedback.loader.present().catch(err=>{});
    let period_time=0;
    if(this.period==31)
      period_time=1000*60*60*24*this.number;
    else if(this.period==52)
      period_time=1000*60*60*24*7*this.number;
    else if(this.period==12)
      period_time=1000*60*60*24*30*this.number;
    delete this.task.deadline;
    this.task.period=period_time;
    this.task.end_recurring=this.end_date?(+this.end_date):-1,
    this.task.start_recurring=this.start_date?(+this.start_date) : +new Date(),
    this.task.next_time=(this.task.start_recurring + period_time)
    this.utilityService.recurringTask(this.task).subscribe(data=>{
      this.userFeedback.loader.dismiss().catch(err=>{});
      this.userFeedback.make_toast("recurring task was created successfully");
      this.navCtrl.setRoot(DashboardPage)
    })
  }

  ionViewDidLeave()
  {
    MyApp.i = 0;
    this.utilityService.decrease_badge();
  }
}
