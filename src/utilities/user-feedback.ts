import {LoadingController,Loading,AlertController } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';
import { LocalNotifications } from '@ionic-native/local-notifications';
import {Storage} from '@ionic/storage';
import { Events } from 'ionic-angular';
import {DashboardPage} from '../pages/dashboard/dashboard';

@Injectable()
export class UserFeedback{
  loader:Loading;
  icon:any;
  user:any={};


 constructor(private events:Events,private storage:Storage,private localNotifications:LocalNotifications,public toastCtrl: ToastController,public alertCtrl:AlertController,public loadingCtrl:LoadingController){

   storage.get('userInfo').then(user=>{
     this.user=user;
   });
   this.loader = this.loadingCtrl.create({
       content: "Please wait...",
       dismissOnPageChange:true
   });
   this.convertToDataURLviaCanvas('assets/icon.png','image/png').then((data)=>{
     this.icon=data;
   });
 }

 start_progress(){
    this.loader=this.loadingCtrl.create({
        content: "Please wait...",
        dismissOnPageChange:true
   });
   this.loader.present();
 }

 close_progress(){
   this.loader.dismiss();
 }

 alert(title:string,subtitle:string) {
    let msg = this.alertCtrl.create({
     title: title,
     subTitle: subtitle,
     buttons: ['Okay']
    });
    msg.present().catch(err=>{});
 }

 make_toast(msg:string,parent_id:any=0,task_id:any=0){
   let self=this;
   let toast = this.toastCtrl.create({
      message: msg,
      duration: 4000,
      position: 'top',
      closeButtonText: 'X',
      cssClass:"toast",
      showCloseButton: true,
      //dismissOnPageChange:true
    });
    toast.present().then(()=>{
      let element=document.getElementsByClassName('toast-container')[0];
        element.addEventListener('click',function(e){
          if((parent_id || task_id) && !(e.target instanceof HTMLSpanElement))
            toast.dismiss();
            self.events.publish('get_task_from_toast',{
              parent_id:parent_id,child_id:self.user._id,
              info:task_id
            });
        });
    }).catch(err=>{});
 }


 presentConfirm(cb) {
  let alert = this.alertCtrl.create({
    title: 'Confirm',
    message: 'Are You Sure ?',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel'
      },
      {
        text: 'Ok',
        handler: () => {
          cb();
        }
      }
    ]
  });
  alert.present().catch(err=>{});
}

  generate_notification(id,title,parent_id:any=0,task_id:any=0){
    /*this.localNotifications.schedule({
      id: id,
      title:title,
      icon:this.icon,
      smallIcon:this.icon
    });*/
  if(DashboardPage.counter == 0)
  {
    this.make_toast(title,parent_id,task_id);
    DashboardPage.counter = 1;
  }  
  }

  convertToDataURLviaCanvas(url, outputFormat){
  	return new Promise( (resolve, reject) => {
  		let img = new Image();
  		img.crossOrigin = 'Anonymous';
  		img.onload = function(){
  			let canvas = <HTMLCanvasElement> document.createElement('CANVAS'),
  			ctx = canvas.getContext('2d'),
  			dataURL;
  			canvas.height = 50;
  			canvas.width = 50;
  			ctx.drawImage(img, 0, 0);
  			dataURL = canvas.toDataURL(outputFormat);
  			//callback(dataURL);
  			canvas = null;
  			resolve(dataURL);
  		};
  		img.src = url;
  	});
  }
}
